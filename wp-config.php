<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define( 'JETPACK_DEV_DEBUG', true);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_trendfollowing');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_c*7+I3[87iZ5Sz&&.3F~r:a-|lu]?,!)EXgKkRK2CFTU`%IPC_PNf|mJ]a+5Kj~');
define('SECURE_AUTH_KEY',  'adTfnrq^Lz?(?v)scSQ!GdX24m=A>2`qh^cr[|t+&$$Ap6M}Hnqawz93TD.R?^l0');
define('LOGGED_IN_KEY',    'S8}5AfmU($FLk~oDAEafDjzyBt2+ZThsCc/wkOu3PP$|vwAkT}pd:4D:2GwARoBM');
define('NONCE_KEY',        'U=~$/r!g=}TLOpd<d?]gok.#A$K5Kix[S8z/4u+?#xa_w8WMZ,NB68t;>LbofrmM');
define('AUTH_SALT',        '(cC`*Cx)<)hrOKV@(nf*0`VS/`!HW!8!dw+##Zl)@Z20ZUbefxEwP=rPa~3HV$Et');
define('SECURE_AUTH_SALT', 'g;y3a,SpAK!+@4<@[f9`n}.pDU/|3gX^q}zQk*EM|>ew6WqNO4ziS9;StM@]Nv0-');
define('LOGGED_IN_SALT',   'd*)o#b,dehs#&#G} SdY;g?ke_igs+,K(^:Se*[j3,:[E+2T+>#d/@.$.eFMJj#6');
define('NONCE_SALT',       '$IUeSt7!p}gMp]i|Xw]Y3C|W.ctd<iekf8G(f_|F2 j~l2dwx7hRp6xmxgs[j^NX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('AUTOSAVE_INTERVAL', 300); // seconds
define('WP_POST_REVISIONS', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
