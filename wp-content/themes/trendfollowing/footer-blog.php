<?php
/**
 * The template for displaying the blog footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trend_Following
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="container">
            <nav class="footer-nav">
                <div class="menu-box">
                    <?php
                        $locations = get_nav_menu_locations();
                        $menu = wp_get_nav_menu_object( $locations[ 'footer-1' ] );
                        $menu2 = wp_get_nav_menu_object( $locations[ 'footer-2' ] );
                    ?>
                    <p class="menu-name"><?php echo $menu->name; ?></p>
                    <?php wp_nav_menu( array( 
                        'theme_location' => 'footer-1',
                        'menu_id' => 'footer1-menu'
                    ) ); ?>
                </div>
                <div class="menu-box">
                    <p class="menu-name"><?php echo $menu2->name; ?></p>
                    <?php wp_nav_menu( array( 
                        'theme_location' => 'footer-2',
                        'menu_id' => 'footer2-menu'
                    ) ); ?>
                </div>
                <div class="social-media">
                    <a class="google-plus" href="<?php echo get_field( 'social_google', 140 ); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a class="facebook" href="<?php echo get_field( 'social_facebook', 140 ); ?>"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                    <a class="linked-in" href="<?php echo get_field( 'social_linkedin', 140 ); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a class="twitter" href="<?php echo get_field( 'social_twitter', 140 ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
		    </nav>
            <div class="site-info">
                <p class="disclaimer"><a href="http://www.trendfollowing.com/disclaimers">Read Disclaimer</a></p>
                <p class="copyright">Copyright <a href="http://www.trendfollowing.com">Trend Following</a> 2016</p>
            </div>
        </div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
