<?php
/**
 * Trend Following functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Trend_Following
 */

if ( ! function_exists( 'tf_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tf_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Trend Following, use a find and replace
	 * to change 'tf' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'tf', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'tf' ),
        'secondary' => esc_html__( 'Secondary', 'tf' ),
        'footer-1' => esc_html__( 'Footer 1', 'tf' ),
        'footer-2' => esc_html__( 'Footer 2', 'tf' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tf_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'tf_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tf_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tf_content_width', 640 );
}
add_action( 'after_setup_theme', 'tf_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tf_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tf' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tf_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tf_scripts() {
    wp_enqueue_style( 'tf-fontawesome', get_template_directory_uri() . '/css/font-awesome/css/font-awesome.min.css' );

    if ( is_front_page() || is_page() ) {
        wp_enqueue_style( 'tf-lightslider-style', get_template_directory_uri() . '/js/lightslider-master/dist/css/lightslider.min.css' );

        wp_enqueue_script( 'tf-lightslider', get_template_directory_uri() . '/js/lightslider-master/dist/js/lightslider.min.js', array( 'jquery' ) );
    }

    wp_enqueue_style( 'tf-style', get_stylesheet_uri() );

    wp_enqueue_script( 'tf-script', get_template_directory_uri() . '/js/tf.js',  array( 'jquery' ) );

	wp_enqueue_script( 'tf-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'tf-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tf_scripts' );

/**
 * Create testimonial custom post type.
 */
function tf_testimonial_post_type() {
  register_post_type( 'tf_testimonials',
    array(
      'labels' => array(
        'name' => __( 'Testimonials', 'tf' ),
        'singular_name' => __( 'Testimonial', 'tf' ),
        'add_new_item' => __( 'Add New Testimonial', 'tf' ),
        'edit_item' => __( 'Edit Testimonial', 'tf' ),
        'new_item' => __( 'New Testimonial', 'tf' ),
        'search_items' => __( 'Search Testimonials', 'tf' ),
        'not_found' => __( 'No testimonials found', 'tf' ),
        'not_found_in_trash' => __( 'No testimonials found in Trash', 'tf' ),
        'all_items' => __( 'All Testimonials', 'tf' ),
        'archives' => __( 'Testimonial Archives', 'tf' ),
      ),
      'public' => true,
      'has_archive' => true,
      'exclude_from_search' => true,
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'rewrite' => array( 'slug' => 'testimonials' ),
    )
  );
}
add_action( 'init', 'tf_testimonial_post_type' );

/**
 * Get webinar data.
 */
function tf_get_webinar_api_data() {
    $api_url = 'https://app.webinarjam.com/api/v2/ever/webinar';
    $response = wp_remote_post( $api_url, array(
        'body' => array( 
            'api_key' => 'd45c2c286225e4d491265534c14c79d131988eaed07b3653b0225c615b211c08',
            'webinar_id' => 'ff67748ed8',
            ),
    ) );

    if ( $response['response']['code'] == 200 ) {
        $data = json_decode( $response['body'] );

        // Get the user timezone from webinar data
        $timezone = new DateTimeZone( $data->webinar->timezone );
        $offset = $timezone->getOffset( new DateTime );
        $default_GMT = $offset / 3600; // GMT offset
        $user_GMT = isset( $_COOKIE['user_timezone'] ) ? ( (int)$_COOKIE['user_timezone'] / 60 ) * -1 : 0;
        $user_timezone_name = timezone_name_from_abbr( '', ( $user_GMT * 3600 ), 1 );
        
        // Workaround for bug #44780-https://bugs.php.net/bug.php?id=44780
        if( $user_timezone_name === false ) 
            $user_timezone_name = timezone_name_from_abbr('', ( $user_GMT * 3600 ), 0);

        $newDates = [];
        $form_dates = [];

        // If cookie for user's timezone is not set, use default dates
        if ( ! isset( $_COOKIE['user_timezone'] ) ) {
            foreach( $data->webinar->schedules as $sch ) {
                $form_dates[] = [
                    'date' => $sch->date . ' ' . $data->webinar->timezone,
                    'schedule' => $sch->schedule
                ];
            }

            return [ 'form_dates' => $form_dates ];
        }

        // Convert each date to match user's timezone'
        foreach ( $data->webinar->schedules as $sched ) {
            $date = $sched->date;
            $days_arr = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            $mths_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $weekday = '';
            $month = '';
            $day_num = '';
            
            // Extract weekday
            $date_arr = explode( ' ', $date );           
            $date_arr = array_map( function ($str) {
                return rtrim( $str, ',' );
            }, $date_arr);

            foreach( $days_arr as $d ) {
                if ( in_array( $d, $date_arr ) ) {
                    $weekday = $d;
                    break;
                }
            }

            // Extract month
            foreach( $mths_arr as $m ) {
                if ( in_array( $m, $date_arr ) ) {
                    $month = $m;
                    break;
                }
            }

            // Extract day number
            if ( ! empty( $month ) ) {
                $month_index = array_search( $month, $date_arr );
                $day_num = $date_arr[$month_index - 1];
            }

            // Extract time
            $time_pattern = '/((1[0-2]|0[0-9]|[0-9]):[0-5][0-9]\s(PM|AM))/i';
            preg_match( $time_pattern, $date, $time_matches, PREG_OFFSET_CAPTURE );
            
            $time = substr( $date, $time_matches[0][1] );
            $year = date('Y');
            $seconds = ( $user_GMT - $default_GMT ) * 3600;

            if ( ! empty( $weekday ) && ! empty( $month ) ) {
                $given_date = $weekday . ' ' . $month . ' ' . $day_num . ' ' . $year . ' ' . $time;
            } elseif ( ! empty( $weekday ) ) {
                $given_date = $weekday . ' ' . $time;
            } elseif ( ! empty ( $month ) ) {
                $given_date = $month . ' ' . $day_num . ' ' . $year . ' ' . $time;
            } elseif ( empty( $weekday ) && empty( $month ) ) {
                $given_date = $time;
            }

            $date_time = new DateTime( $given_date );

            $seconds > 0 ? $date_time->add( new DateInterval( 'PT0' . $seconds . 'S' ) ) : $date_time->sub( new DateInterval( 'PT0' . ( $seconds * -1 ) . 'S' ) );

            if ( ! empty( $weekday ) ) {
                $new_date = $date_time->format('l, j M h:i A');
                $rep = str_replace( $weekday . ', ' . $day_num . ' ' . $month . ' ' . $time, $new_date, $date );
            } elseif ( ! empty( $month ) ) {
                $new_date = $date_time->format('j M h:i A');
                $rep = str_replace( $day_num . ' ' . $month . ' ' . $time, $new_date, $date );
            } else {
                $new_date = $date_time->format('h:i A');
                $rep = str_replace( $time, $new_date, $date );
            }
            
            $form_dates[] = [ 
                'date' => $rep . ' ' . $user_timezone_name,
                'schedule' => $sched->schedule 
            ];

        } // foreach $data->webinar->schedules

        return [ 'form_dates'=> $form_dates ];
    } //if $response['response']['code'] == 200

    return [];
}

/**
 * Register user for webinar.
 */
function tf_webinar_register_user(  $schedules = [] ) {
    $errors = '';
    $dates = [];

    // Validate fields
    if ( empty( $_REQUEST['first_name'] ) || empty( $_REQUEST['last_name'] ) || empty( $_REQUEST['email' ] ) ) {
        $errors = '<p>All fields are required!!</p>';
    } elseif ( ! filter_var( $_REQUEST['email' ], FILTER_VALIDATE_EMAIL ) ) {
        $errors = '<p>Please enter a valid email address</p>';
    }

    // Validate date
    if ( ! empty ( $schedules ) ) {
        foreach ( $schedules as $val ) {
            $dates[] = $val->schedule;
        }
    }

    if ( isset( $_REQUEST['date'] ) && ( $_REQUEST['date'] == 'placeholder' || ! in_array( $_REQUEST['date' ], $dates ) ) )  {
        $errors = '<p>Please select a valid date</p>';
    }

    if ( $errors ) {
        return $errors;
    }

    // Register the user
    $api_url = 'https://app.webinarjam.com/api/v2/ever/register';
    $response = wp_remote_post( $api_url, array(
        'body' => array( 
            'api_key'    => 'd45c2c286225e4d491265534c14c79d131988eaed07b3653b0225c615b211c08',
            'webinar_id' => 'ff67748ed8',
            'name'       => $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'],
            'email'      => $_REQUEST['email'],
            'schedule'   => $_REQUEST['date'],
        ),
    ) );

    if ( $response['response']['code'] == 200 ) {
        $data = json_decode($response['body']);

        wp_redirect( $data->user->thank_you_url );
        exit;
    }

    return '<p>The form could not be processed.</p>';
}

/* Remove default Jetpack sharing icons position */
function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}
add_action( 'loop_start', 'jptweak_remove_share' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
