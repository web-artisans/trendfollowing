<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trend_Following
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="https://use.typekit.net/trb2wnj.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
    $class = is_page( 'pricing' ) ? 'pricing-page' : '';
?>

<div id="page" class="site <?php echo $class; ?>">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'tf' ); ?></a>

    <header class="secondary">
        <div class="small-logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php the_field( 'header_logo_small', 5 ); ?>"></a>
        </div>
        <div class="small-menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <nav class="secondary-nav" role="navigation">
			<?php wp_nav_menu( array( 
                'theme_location' => 'secondary',
                'menu_id' => 'secondary-menu',
                'menu_class' => 'clear',
                'container_class' => 'clear' 
            ) ); ?>
		</nav>
    </header>

	<header id="masthead" class="site-header" role="banner">
        <div class="box">
            <div class="site-branding">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php the_field( 'header_logo', 5 ); ?>"></a>
            </div>

            <div class="small-menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <nav id="site-navigation" class="main-navigation" role="navigation">
                <?php wp_nav_menu( array( 
                    'theme_location' => 'primary',
                    'menu_id' => 'primary-menu',
                    'menu_class' => 'clear' 
                ) ); ?>
            </nav><!-- #site-navigation -->
        </div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
