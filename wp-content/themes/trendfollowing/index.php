<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trend_Following
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <div class="top-featured">
                <?php 
                    $class = '';
                    $sidebar = get_field( 'show_sidebar', 140 ) ? 'show-sidebar' : 'hide-sidebar';

                    if ( get_field( 'show_featured', 140 ) && has_post_thumbnail( 140 ) ) :
                        $class = 'has-featured';
                        echo get_the_post_thumbnail( 140 );
                    endif; 
                ?>
            </div>
		<?php
		if ( have_posts() ) :?>

            <div class="main-container <?php echo $class; ?>">
                <div class="posts <?php echo $sidebar; ?>">
                    <?php while ( have_posts() ) : the_post();

                        get_template_part( 'template-parts/content', get_post_format() );

                    endwhile;
                    
                    the_posts_navigation(); ?>
                </div><!--.posts-->
                <?php if ( $sidebar == 'show-sidebar') :
                    get_sidebar(); 
                endif; ?>
            </div><!-- .container -->

		<?php else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
            
            <?php get_template_part( 'template-parts/content', 'cta' ); ?>
            <?php get_template_part( 'template-parts/content', 'optin-bar' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer( 'blog' );
