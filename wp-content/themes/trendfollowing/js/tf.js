jQuery( document ).ready( function( $ ) {
    // Store timezone in localStorage
    document.cookie = 'user_timezone=' + new Date().getTimezoneOffset();

    // Small/Tablet Menu
    $( '.site-header .small-menu' ).click( function(evt) {
        $( '.main-navigation' ).slideToggle(500);
    });

    // Mobile Menu
    $( '.secondary .small-menu' ).click( function(evt) {
        $( '.secondary-nav' ).slideToggle(500);
    });

    // Fixed Nav
    $( window ).scroll( function() {
        var $currPos = $( this ).scrollTop();

        if ( $currPos > 41 ) {
            $( '.secondary' ).addClass( 'hidden' );
            $( '.site-header .box' ).addClass( 'fixed-nav' );
            $( '.site-header' ).css( 'margin-bottom', 41 );
        }
        else {
            $( '.secondary' ).removeClass( 'hidden' );
            $( '.site-header .box' ).removeClass( 'fixed-nav' );
            $( '.site-header' ).css( 'margin-bottom', 0 );
        }
    });

    // Optin Bar
    $( window ).scroll( function() {
        var $currPos = $( this ).scrollTop();

        if ( localStorage.hideOptin != 'true' && $currPos > 840 ) {          
            $( '.optin-bar' ).addClass( 'show' ).animate({
               bottom: '0'
            }, 1000);
        }
    });

    // Hide Optin Bar
    $( '.optin-bar .fa-times' ).on( 'click', function() {
        $( this ).parent().hide();
        localStorage.hideOptin = true;
    });

    // Masthead Bottom Links
    $( '.scroll' ).on( 'click', function(evt) {      
        var target = this.hash;

        if ( target.charAt(0) == '#' ) {
            evt.preventDefault();

            $('html, body').animate({
                scrollTop: $( target ).offset().top
            }, 1000);
        }
    });

    // Resources Section
    var start = 0;
    var count = $( '.resource-list li' ).length - 1;
    var interval = setInterval( showResources, 20000 );

    $( '.resource-list li' ).hover(
        function() {
            $self = $( this );

            clearInterval(interval);

            $( '.resource-list .active' ).removeClass( 'active' ).addClass( 'inactive' );

            $self.addClass( 'active' ).removeClass( 'inactive' );

            $( '.resource-data > .active' ).fadeOut( 500, function() {
                $( this ).removeClass( 'active' );
                $( '.resource-data > div' ).eq( $self.index() ).addClass( 'active' ).fadeIn( 500 );
            });
        }, function() {
            start = $( this ).index();
            interval = setInterval( showResources, 20000 );        
        }
    );

    $( '.resource-data' ).hover(
        function() {
            clearInterval(interval);
        }, function() {
            interval = setInterval( showResources, 20000 );        
        }
    );

    function showResources() {
        if ( start == count ) {
            $( '.resource-list .active' ).removeClass( 'active' ).addClass( 'inactive' );

            $( '.resource-list li' ).eq(0).addClass( 'active' ).removeClass( 'inactive' );

            $( '.resource-data .active' ).fadeOut( 500, function() {
                $( this ).removeClass( 'active' );
                $( '.resource-data > div' ).eq(0).addClass( 'active' ).fadeIn( 500 );
            });

            start = 0;
        } else {
            $( '.resource-list .active' ).removeClass( 'active' ).addClass( 'inactive' ).next().addClass( 'active' ).removeClass( 'inactive' );

            $( '.resource-data > .active' ).fadeOut( 500, function() {
                $( this ).removeClass( 'active' ).next().addClass( 'active' ).fadeIn( 500 );
            });

            start++;
        }   
    }

    /* Move flagship package ahead of basic for small devices */
    if ( $( window ).width() <= 797 ) {
        var $listItems = $( '.packages > li' );
        $listItems.eq(1).clone().prependTo( '.packages' );
        $listItems.eq(1).remove();
        $listItems.eq(0).removeClass( 'first' ).addClass( 'third' );
    }

    // Testimonials Slideshow
    if ( $( '#lightSlider' ).length != 0 ) {
        $( '#lightSlider' ).lightSlider({
            pager: true,
            auto: true,
            pause: 15000,
            speed: 800,
            pauseOnHover: true,
            prevHtml: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            nextHtml: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            item: 1,
            loop: true,
            slideMargin: 0,
            keyPress: true,
        });
    }

});