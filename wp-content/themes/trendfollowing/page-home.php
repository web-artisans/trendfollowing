<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trend_Following
 */

set_query_var( 'errors', '' );

// Get webinar data for form
set_query_var( 'data', tf_get_webinar_api_data() );

// Validate and register user for webinar
if ( ! empty( $_REQUEST ) ) {
    set_query_var( 'errors', tf_webinar_register_user( get_query_var('data')['schedules'] ) );
}

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page-home' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
