<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trend_Following
 */

if ( get_field( 'subtitle' ) ) : 
    $class = '';
else :
    $class = 'no-subtitle';
endif;
?>

<div class="sidebar <?php echo $class; ?>">
    <div class="cta-flagship">
        <p class="sub"><?php the_field( 'cta1_sub', 140 ); ?></p>
        <p class="heading"><?php the_field( 'cta1_heading', 140 ); ?></p>
        <a class="button" href="<?php the_field( 'cta1_link', 140 ); ?>"><?php the_field( 'cta1_link_text', 140 ); ?></a>
    </div>
    <div class="books">
        <p class="heading"><?php the_field( 'sbar_books_heading', 140 ); ?></p>
        <div class="box">
        <?php foreach( get_field( 'sbar_books', 140 ) as $book ) : ?>
            <a href="<?php echo $book['sbar_book_link']; ?>"><img src="<?php echo $book['sbar_book_image']; ?>"></a>
        <?php endforeach; ?>
        </div>
    </div>
    <div class="film">
        <p class="heading"><?php the_field( 'sbar_film_heading', 140 ); ?></p>
        <div class="the-video">
            <?php the_field( 'sbar_film', 140 ); ?>
        </div>
    </div>
    <div class="cta-webinar">
        <p class="sub"><?php the_field( 'cta2_sub', 140 ); ?></p>
        <p class="heading"><?php the_field( 'cta2_heading', 140 ); ?></p>
        <a  class="button" href="<?php the_field( 'cta2_link', 140 ); ?>"><?php the_field( 'cta2_link_text', 140 ); ?></a>
    </div>
</div>
