<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Trend_Following
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <div class="top-featured">
                <?php 
                    $sidebar = get_field( 'show_sidebar', 140 ) ? 'show-sidebar' : 'hide-sidebar';

                    if ( has_post_thumbnail() ) :
                        the_post_thumbnail();
                    endif;
                ?>
            </div>		
            <div class="main-container <?php echo $sidebar; ?>">
                    <div class="post-box <?php echo $sidebar; ?>">
                        <?php while ( have_posts() ) : the_post();

                            get_template_part( 'template-parts/content', get_post_format() );

                            the_post_navigation(); ?>
                    </div><!-- .posts-->
                    <?php 
                        if ( $sidebar == 'show-sidebar') :
                            get_sidebar(); 
                        endif;
                    // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>       
            </div><!---main-container-->
            <?php get_template_part( 'template-parts/content', 'cta' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer( 'blog' );
