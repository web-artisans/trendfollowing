<section class="weekly-updates" style="background: url(<?php the_field( 'updates_bg', 5 ); ?>) top center no-repeat">
    <div class="container">
        <img src="<?php the_field( 'updates_heading', 5 ); ?>">
        <p class="desc"><?php the_field( 'updates_desc', 5 ); ?></p>
        <form action="">
            <div class="join">
                <img src="<?php echo get_field( 'updates_email_icon', 5 ); ?>">
                <input type="text" name="email" placeholder="Email Address">
                <input type="submit" name="submit" value="Join">
            </div>
        </form>
        <a href="#">Join</a>
        <ul class="links">
            <?php foreach( get_field( 'updates_menu', 5 ) as $item ) : ?>
                <li>
                    <a href="<?php echo $item['updates_link']; ?>"><img src="<?php echo $item['updates_icon']; ?>"></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>