<section class="optin-bar">
    <div class="join">
        <p><?php the_field( 'optin_text', 5 ); ?></p>
        <div class="box">
            <img src="<?php echo get_field( 'optin_icon', 5 ); ?>">
            <input type="text" name="email" placeholder="Email Address">
            <input type="submit" name="submit" value="Join">
        </div>
        <a href="#">Join</a>
    </div>
    <i class="fa fa-times" aria-hidden="true"></i>
</section>