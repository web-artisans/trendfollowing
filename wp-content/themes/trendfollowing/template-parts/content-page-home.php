<?php
/**
 * Template part for displaying page content in page-home.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trend_Following
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" style="background: url(<?php the_post_thumbnail_url(); ?>) top center no-repeat">
        <section class="webinar-form">
            <div class="box">
                <p class="small-heading"><?php the_field( 'header_sub' ); ?></p>
                <h1><?php the_field( 'header_title' ); ?></h1>
                <p class="desc"><?php the_field( 'header_desc' ); ?></p>
                <div class="error">
                    <?php echo $errors; ?>
                </div>
            </div>
            <form id="register" action="">
                <div class="full_name">
                    <p class="first_name">
                        <input type="text" name="first_name" id="first_name" placeholder="First Name" value="<?php echo isset( $_REQUEST['first_name'] ) ? $_REQUEST['first_name'] : ''; ?>" required>
                    </p>
                    <p class="last_name">
                        <input type="text" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo isset( $_REQUEST['last_name'] ) ? $_REQUEST['last_name'] : ''; ?>" required>
                    </p>
                </div>
                <p class="email">
                    <input type="email" name="email" id="email" placeholder="Email Address" value="<?php echo isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : ''; ?>" required>
                </p>
                <div class="date-time">
                    <select id="date" name="date">
                        <option value="placeholder">Choose Date and Time</option>
                            <?php 
                            if ( ! empty( $data ) ) :
                                foreach( $data['form_dates'] as $val ): ?>
                                    <option data-date="<?php echo $val['date']; ?>" value="<?php echo $val['schedule']; ?>" <?php if ( isset( $_REQUEST['date'] ) ) { selected( $_REQUEST['date'], $val['schedule'] ); } ?>><?php echo $val['date']; ?></option>
                                <?php endforeach; 
                            endif; ?>
                    </select>
                </div>
                <div class="sign-up">
                    <p class="gap"></p>
                    <input name="submit" type="submit" value="Sign Up Now">
                </div>
            </form>
        </section>
        <div class="wrapper">
            <ul>
                <?php foreach( get_field('header_info_list') as $i => $row ) : ?>
                    <li>
                        <?php if ( $i == 2 ) : ?>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <?php else : ?>
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        <?php endif; ?>
                        <a href="<?php echo $row['header_target']; ?>" class="text scroll"><?php echo $row['header_list_text']; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
	</header><!-- .entry-header -->

	<section class="how-it-works">
        <div class="container">
            <div class="box">
                <p class="small-heading"><?php the_field( 'how_sub' ); ?></p>
                <h1><?php the_field( 'how_heading' ); ?></h1>
                <p class="desc"><?php the_field( 'how_desc' ); ?></p>
            </div>
            <img class="main-image" src="<?php the_field( 'how_image' ); ?>">
            <div class="content">
                <h2><?php the_field( 'how_content_heading' ); ?></h2>
                <div><?php the_field( 'how_content' ); ?></div>
                <a href="<?php the_field( 'how_link' ); ?>"><?php the_field( 'how_link_text' ); ?></a>
            </div>
        </div>
	</section>

	<section id="real-results" class="customers">
        <div class="container">
            <div class="box">
                <p class="small-heading"><?php the_field( 'cust_sub' ); ?></p>
                <h1><?php the_field( 'cust_heading' ); ?></h1>
                <p class="desc"><?php the_field( 'cust_desc' ); ?></p>
            </div>
            <ul>
                <?php foreach( get_field( 'cust_data' ) as $row ) : ?>
                    <li>
                        <img src="<?php echo $row['cust_image']; ?>">
                        <div class="text"><?php echo $row['cust_text']; ?></div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <a class="cta" href="<?php the_field( 'cust_button_link' ); ?>"><?php the_field( 'cust_button_text' ); ?></a>
        </div>
	</section>

    <?php get_template_part( 'template-parts/content', 'testimonials' ); ?>

	<section id="explore" class="resources">
        <div class="container">
            <div class="box">
                <p class="small-heading"><?php the_field( 'res_sub' ); ?></p>
                <h1><?php the_field( 'res_heading' ); ?></h1>
                <p class="desc"><?php the_field( 'res_desc' ); ?></p>
            </div>
            <ul class="small-devices">
                <?php foreach( get_field( 'res_list' ) as $index => $row ) : ?>
                <li>
                    <div class="wrapper">
                        <img src="<?php echo $row['res_icon_active']; ?>">
                        <a href="<?php echo $row['res_link_small']; ?>"><h2><?php echo $row['res_title']; ?></h2></a>
                    </div>
                    <div class="text"><?php echo $row['res_text']; ?></div>
                </li>
                <?php endforeach; ?>
            </ul>
            <div class="resource-list">
                <ul>
                    <?php foreach( get_field( 'res_list' ) as $index => $row ) : ?>
                        <li class="<?php echo $index == 0 ? 'active' : 'inactive'; ?>">
                            <img class="grey" src="<?php echo $row['res_icon']; ?>">
                            <img class="blue" src="<?php echo $row['res_icon_active']; ?>">
                            <div class="wrapper">
                                <h2><?php echo $row['res_title']; ?></h2>
                                <div class="text"><?php echo $row['res_text']; ?></div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="resource-data">
                <div class="podcast active">
                    <p class="heading"><?php echo get_field( 'pod_heading' ); ?></p>
                    <ul>
                        <?php foreach( get_field( 'podcast_res' ) as $data ) : ?>
                            <li>
                                <a class="image" href="<?php echo $data['pod_link']; ?>"><img src="<?php echo $data['pod_image']; ?>"></a>
                                <a class="author" href="<?php echo $data['pod_link']; ?>"><h2><?php echo $data['pod_name']; ?></h2></a>
                                <p class="title"><?php echo $data['pod_title']; ?></p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="books">
                     <p class="heading"><?php echo get_field( 'book_heading' ); ?></p>
                    <ul>
                        <?php foreach( get_field( 'book_res' ) as $data ) : ?>
                            <li>
                                <a href="<?php echo $data['book_link']; ?>"><img src="<?php echo $data['book_image']; ?>"></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="speeches">
                    <p class="heading"><?php echo get_field( 'sp_heading' ); ?></p>
                    <a href="<?php echo get_field( 'sp_btn_link' ); ?>"><img src="<?php echo get_field( 'sp_image' ); ?>"></a>
                    <a class="cta" href="<?php echo get_field( 'sp_btn_link' ); ?>"><?php echo get_field( 'sp_btn_text' ); ?></a>
                </div>
                <div class="films">
                    <p class="heading"><?php echo get_field( 'film_heading' ); ?></p>
                    <div class="video"><?php the_field( 'film_video' ); ?></div>
                </div>
            </div><!-- .resource-data -->
        </div><!-- .container -->
	</section><!-- .resources -->

    <?php get_template_part( 'template-parts/content', 'pricing' ); ?>

    <section class="contact-webinar">
        <div class="container">
            <ul>
                <?php foreach( get_field( 'call_to_action' ) as $row ) : ?>
                    <li>
                        <img src="<?php echo $row['cta_icon']; ?>">
                        <h1><?php echo $row['cta_heading']; ?></h1>
                        <p class="desc"><?php echo $row['cta_desc']; ?></p>
                        <a class="cta" href="<?php echo $row['cta_button_link']; ?>"><?php echo $row['cta_button_text']; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>

    <?php get_template_part( 'template-parts/content', 'cta' ); ?>
    <?php get_template_part( 'template-parts/content', 'optin-bar' ); ?>
</article><!-- #post-## -->
