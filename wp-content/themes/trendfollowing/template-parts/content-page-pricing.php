<?php
/**
 * Template part for displaying page content in page-pricing.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trend_Following
 */

?>

<?php get_template_part( 'template-parts/content', 'pricing' ); ?>
<?php get_template_part( 'template-parts/content', 'testimonials' ); ?>
<?php get_template_part( 'template-parts/content', 'optin-bar' ); ?>
