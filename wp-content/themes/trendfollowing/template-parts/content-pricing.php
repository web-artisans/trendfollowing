    <section class="pricing">
        <header>
            <p class="small-heading"><?php the_field( 'pricing_sub', 5 ); ?></p>
            <h1><?php the_field( 'pricing_heading', 5 ); ?></h1>
        </header>
        <div class="container">
            <div class="pricing-table">
                <ul class="packages">
                    <?php foreach( get_field( 'price_packages', 5 ) as $i => $row ) : 
                        if ( $i == 0 ) : $class = 'first';
                        elseif ( $i == 1 ) : $class = 'second';
                        else : $class = 'third';
                        endif;
                    ?>
                        <li class="<?php echo $class; ?>">
                            <div class="box">
                                <h2><?php echo $row['package_name']; ?></h2>
                                <p class="pkg"><?php echo $row['package_name_sm']; ?></p>
                                <p class="price"><?php echo $row['package_price']; ?></p>
                                <a class="cta" href="<?php echo $row['buy_button_link']; ?>"><?php echo $row['buy_button_text']; ?></a>
                            </div>
                            <h3><?php echo $row['features_title']; ?></h3>
                            <ul class="features">
                                <?php foreach( $row['package_features'] as $feat ) : ?>
                                    <li class="<?php echo $feat['feature_status'] ? 'inactive' : 'active' ; ?>">
                                        <img class="active" src="<?php echo get_field( 'pricing_icon_active', 5 ); ?>">
                                        <img class="inactive" src="<?php echo get_field( 'pricing_icon_inactive', 5 ); ?>">
                                        <p class="text"><?php echo $feat['feature']; ?></p>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div><!--.container-->
    </section><!--.pricing-->