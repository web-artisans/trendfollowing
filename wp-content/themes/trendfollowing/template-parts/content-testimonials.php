<section class="testimonials">
    <div class="container">
        <ul id="lightSlider" class="te-slider">
            <?php $testimonials = get_field( 'testimonials', 5 ); ?>
            <?php foreach ( $testimonials as $data ) : ?>
            <li class="slide">
                <div class="img-box">
                    <?php echo get_the_post_thumbnail( $data->ID, 'thumbnail' ); ?>
                </div>
                <div class="content">
                    <p class="quote"><?php echo $data->post_content; ?></p>
                    <p class="author"><?php echo $data->post_title; ?></p>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>