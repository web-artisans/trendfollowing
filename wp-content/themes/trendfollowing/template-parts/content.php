<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trend_Following
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
            <?php
                the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
                
                if ( get_field( 'post_subtitle' ) ) : ?>
                    <h3 class="subtitle"><?php the_field('post_subtitle'); ?></h3>
                <?php endif;

            if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta">
                <?php printf( __('Posted on %1$s, by %2$s', 'tf' ), substr( get_the_date(), 0, -6 ), get_the_author() ) ?>
            </div><!-- .entry-meta -->
            <?php
            endif; ?>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <?php if ( ! is_single() ) : ?>
                <div class="featured"><?php the_post_thumbnail(); ?></div>
            <?php endif; ?>
            <?php
                the_content();

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tf' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div><!-- .entry-content -->

        <footer class="entry-footer">
            <?php 
                tf_entry_footer();
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }
            ?>
        </footer><!-- .entry-footer -->
</article><!-- #post-## -->
